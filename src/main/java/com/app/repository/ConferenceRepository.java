package com.app.repository;
import java.time.LocalDate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.app.model.Conference;
import com.app.model.QConference;

@Repository
public interface ConferenceRepository extends JpaRepository<Conference, Long>,
		QueryDslPredicateExecutor<Conference>, QuerydslBinderCustomizer<QConference> {
	@Override
    default public void customize(QuerydslBindings bindings, QConference conference) {
        bindings.bind(conference.name).first((name, value) -> name.containsIgnoreCase(value));
    }
	
	@Query("SELECT p from Conference p WHERE p.id = :id AND (SELECT COUNT(po) FROM PurchaseOrder po "
    		+ "WHERE po.conference.id = p.id AND ((po.startDate <= :to AND  po.startDate >= :from) OR (po.endDate <= :to AND  po.endDate >= :from))) = 0")
    Conference finAvailablebyIdAndbetweenDates(@Param("id") Long id, @Param("from") LocalDate from, @Param("to") LocalDate to);
    
}
