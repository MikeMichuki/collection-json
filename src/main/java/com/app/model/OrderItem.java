package com.app.model;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper=false)
public class OrderItem extends PersistableEntity{
	
	@OneToOne
	Seat seat;
	int quantity;
}