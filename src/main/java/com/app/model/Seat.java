package com.app.model;

import javax.persistence.Entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper=false)
public class Seat extends PersistableEntity{

	int seatNumber;
	String row;
	public enum SeatStatus{
		RESERVED,
		BOOKED,
		AVAILABLE
	}
	public enum SeatType {
		VIP, REGULAR
	}
}

