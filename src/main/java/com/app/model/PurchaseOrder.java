package com.app.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper=false)
public class PurchaseOrder {

	@Id
	@GeneratedValue
	Long id;
	@OneToOne
	Conference conference;
	LocalDate startDate;
	LocalDate endDate;
	@Column(precision = 8, scale = 2)
    BigDecimal cost;
	@Enumerated(EnumType.STRING)
	Status status;
	LocalDate createdDate;
	
}
