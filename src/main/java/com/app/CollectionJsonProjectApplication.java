package com.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

@SpringBootApplication
@EntityScan(basePackageClasses = { CollectionJsonProjectApplication.class, Jsr310JpaConverters.class })
public class CollectionJsonProjectApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(CollectionJsonProjectApplication.class, args);
	}
}
