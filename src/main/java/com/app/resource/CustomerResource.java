package com.app.resource;

import com.app.controllers.CustomerController;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.Getter;
import lombok.Setter;
import util.ResourceSupport;
import util.annotations.CJson;

@Getter
@Setter
public class CustomerResource extends ResourceSupport<Long> {
	public CustomerResource() {
		super(CustomerController.class);
	}

	@CJson(name = "id", prompt = "ID", access = Access.READ_ONLY )
	public Long id;
	
	@CJson(name = "name", prompt = "Name", access = Access.AUTO)
    public String name;
	
	@Override
	public Long getId() {
		return id;
	}
}
