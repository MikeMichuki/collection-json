package com.app.exceptions;

public class ConferenceNotAvailableException extends Exception {
	private static final long serialVersionUID = 1L;

	public ConferenceNotAvailableException(Long id) {
		super(String.format("Conference not avaiable! (Conference id: %d)", id));
	}
}