package com.app.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.assemblers.ConferenceResourceAssembler;
import com.app.model.Conference;
import com.app.repository.ConferenceRepository;

import util.interfaces.BaseService;

@Service
public class ConferenceService implements BaseService<Long, String>{

	@Autowired
	ConferenceRepository plantRepo;
	
	private ConferenceResourceAssembler plantResourceAssembler = new ConferenceResourceAssembler();
	
	@Override
	public String createEntity(String entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteEntity(Long id) {
		Conference plant = plantRepo.findOne(id);
		plantRepo.saveAndFlush(plant);
	}

	@Override
	public String updateEntity(Long id, String entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getSingleEntity(Long id) {
		return plantResourceAssembler.toResourceString(plantRepo.findOne(id));
	}

	@Override
	public String getAllEntities() {
		return plantResourceAssembler.toResourcesString(plantRepo.findAll());
	}
}
