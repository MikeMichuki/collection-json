package com.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.services.ReservationService;

import util.GenericController;
import util.annotations.CJLink;

@RestController
@RequestMapping("/api/reservation")
@CJLink(name = "Reservation", prompt = "Reservations", rel = "Reservations", href = "/api/reservations")
public class ReservationController extends GenericController<Long, String> {

	@Autowired
	public ReservationController(ReservationService service) {
		super(service);
	}
}
