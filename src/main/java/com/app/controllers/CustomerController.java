package com.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.services.CustomerService;

import util.GenericController;
import util.annotations.CJLink;

@RestController
@RequestMapping("/api/customer")
@CJLink(name = "Customer", prompt = "Customers", rel = "Customers", href = "/api/customers")
public class CustomerController extends GenericController<Long, String> {

	@Autowired
	public CustomerController(CustomerService service) {
		super(service);
	}
}
