package com.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.services.ConferenceService;

import util.GenericController;
import util.annotations.CJLink;

@RestController
@RequestMapping("/api/conferences")
@CJLink(name = "Conferences", prompt = "Conferences", rel = "conferences",href="/api/conferences")
public class ConferenceController extends GenericController<Long, String>{
	
	@Autowired
	public ConferenceController(ConferenceService service) {
		super(service);
	}
}
