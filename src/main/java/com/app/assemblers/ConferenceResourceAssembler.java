package com.app.assemblers;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.util.Map;

import com.app.controllers.ConferenceController;
import com.app.model.Conference;
import com.app.resource.ConferenceResource;

import net.hamnaberg.json.Property;
import util.CollectionParserHelper;
import util.ResourceAssemblerSupport;

public class ConferenceResourceAssembler
		extends ResourceAssemblerSupport<Conference, ConferenceResource> {

	public ConferenceResourceAssembler() {
		super(ConferenceController.class, ConferenceResource.class);
	}

	@Override
	public Conference toModel(Conference conference, String Collection)
			throws IOException {
		Map<String, Property> properties = CollectionParserHelper
				.getProperties(Collection);
		conference.setPrice(new BigDecimal(
				CollectionParserHelper.getPropertyValue(properties, "cost")));
		conference.setDescription(CollectionParserHelper
				.getPropertyValue(properties, "description"));
		conference.setName(
				CollectionParserHelper.getPropertyValue(properties, "name"));
		conference.setPrice(new BigDecimal(
				CollectionParserHelper.getPropertyValue(properties, "name")));
		return conference;
	}

	public ConferenceResource toResource(Conference conference) {
		if (conference != null) {
			ConferenceResource entity = createResourceWithId(conference.getId(),conference);
			entity.setId(conference.getId());
			entity.setName(conference.getName());
			entity.setDescription(conference.getDescription());
			entity.setPrice(conference.getPrice());
			return entity;
		} else {
			return null;
		}
	}

	@Override
	public String toResourceString(Conference conference) {
		ConferenceResource conferenceResource = toResource(conference);
		try {
			conferenceResource.toCollection();
		} catch (IllegalArgumentException | IllegalAccessException
				| URISyntaxException e) {
			e.printStackTrace();
		}
		return conferenceResource.buildCollection().toString();
	}

	@Override
	public String toResourcesString(Iterable<Conference> conferences) {
		ConferenceResource conferenceResource = new ConferenceResource();
		try {
			conferenceResource.toCollection(toItems(conferences));
		} catch (IllegalArgumentException | IllegalAccessException
				| URISyntaxException e) {
			e.printStackTrace();
		}
		return conferenceResource.buildCollection().toString();
	}
}
